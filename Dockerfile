FROM node:16.6-buster as build

COPY deemix-gui/package.json deemix-gui/yarn.lock deemix-gui/.yarnrc /deemix-build/deemix-gui/
COPY deemix-gui/server/package.json deemix-gui/server/yarn.lock /deemix-build/deemix-gui/server/
COPY deemix-gui/webui/package.json deemix-gui/webui/yarn.lock /deemix-build/deemix-gui/webui/

WORKDIR /deemix-build/deemix-gui

RUN yarn config set cache-folder /yarn_cache \
	&& yarn install \
	&& yarn --cwd server install \
	&& yarn --cwd webui install

COPY . /deemix-build/

RUN	yarn set-version \
	&& yarn predist \
	&& ./node_modules/.bin/pkg --out-path dist -t node16-linuxstatic ./server/package.json

RUN adduser --home /deemix --no-create-home --gecos "" --disabled-password deemix \
	&& mkdir -p /tmp/deemix-imgs \
	&& mkdir -p /deemix/music \
	&& mkdir -p /deemix/.config/deemix \
	&& chown -R deemix:deemix /deemix /tmp/deemix-imgs

USER deemix
WORKDIR /deemix
EXPOSE 6595

CMD ./deemix/deemix-server --host 0.0.0.0 --port 6595


FROM debian:buster-slim as buster

COPY --from=build /deemix-build/deemix-gui/dist/deemix-server /deemix/deemix-server
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /deemix /deemix
COPY --from=build /tmp/deemix-imgs /tmp/deemix-imgs

USER deemix
WORKDIR /deemix
EXPOSE 6595
CMD ./deemix/deemix-server --host 0.0.0.0 --port 6595


FROM scratch

COPY --from=build /deemix-build/deemix-gui/dist/deemix-server /deemix/deemix-server
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /deemix /deemix
COPY --from=build /tmp/deemix-imgs /tmp/deemix-imgs

USER deemix
WORKDIR /deemix
EXPOSE 6595
CMD ./deemix/deemix-server --host 0.0.0.0 --port 6595
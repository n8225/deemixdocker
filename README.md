## Minimal rootless Dockerfile for https://gitlab.com/RemixDev/deemix-gui

### Usage

podman run -d \
        --replace --name deemix \
        -v /home/kanga/music/deemix/config:/deemix/.config/deemix \
        -v /home/kanga/music/music_storage:/deemix/music:rw,shared \
        -p 6595:6595 \
        --userns=keep-id \
        registry.gitlab.com/n8225/deemixdocker:latest \
        && podman logs -f deemix